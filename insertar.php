<?php

spl_autoload_register(function ($nombreClase) {
    require_once "$nombreClase.php";
});

use clases\elementos\Coche;
use clases\librerias\Conexion;

$conexion = new Conexion([
    'baseDatos' => 'concesionario',
]);

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="estilos/main.css">
</head>

<body>


    <?php

    if (isset($_GET['enviar'])) {

        // Extract hace lo mismo que lo de abajo
        // prefix_same => si tienes una variable que coincide con alguno de los campos del formulario, le pone el prefijo form
        extract($_GET, EXTR_PREFIX_SAME, "form");

        // $marca = $_GET['marca'] ?? '';
        // $bastidor = $_GET['bastidor'] ?? '';
        // $modelo = $_GET['modelo'] ?? '';
        // $cilindrada = $_GET['cilindrada'] ?? 0;

        // Insertamos con SQL
        //$conexion->consulta("INSERT INTO coche (bastidor, marca, modelo, cilindrada) VALUES ('{$bastidor}', '{$marca}', '{$modelo}', {$cilindrada})");

        // Ya tenemos un metodo en la clase Coche para insertar en la BBDD
        // Instanciamos un objeto
        $coche = new Coche([
            'bastidor' => $bastidor,
            'marca' => $marca,
            'modelo' => $modelo,
            'cilindrada' => $cilindrada
        ]);
        // Lo insertamos en la BBDD
        $coche->insertar($conexion);

        //redireccionar la página a index
        header('Location: index.php');
    } else {
        // Cargamos el menu
        require_once '_menu.php';

        // Creamos un coche vacio para que los value del formulario esten vacios
        $coche = new Coche();

        // Cargamos el formulario
        require_once '_form.php';
    }

    ?>
</body>

</html>