<?php

spl_autoload_register(function ($nombreClase) {
    require_once "$nombreClase.php";
});

// Para que funcionen las sesiones
session_start();

use clases\elementos\Coche;
use clases\librerias\Conexion;

// Cargamos el menu
require_once '_menu.php';

$conexion = new Conexion([
    'baseDatos' => 'concesionario',
]);

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="estilos/main.css">
</head>

<body>
    <?php

    // Si se ha pulsado el boton enviar del formulario
    if (isset($_GET['enviar'])) {
        $coche = new Coche([
            'bastidor' => $_GET['bastidor'],
            'marca' => $_GET['marca'],
            'modelo' => $_GET['modelo'],
            'cilindrada' => $_GET['cilindrada'],
            'bastidorViejo' => $_SESSION['bastidor'], // bastidor que está en la base de datos y que se va a cambiar
        ]);
        $coche->actualizar($conexion);

        header("Location: index.php");
    } else {
        // Guardo el bastidor en una variable de sesion
        $_SESSION['bastidor'] = $_GET['bastidor'];

        // Consulta para sacar los datos de un registro filtrando por el bastidor
        $datos = $conexion
            ->consulta("
        SELECT * FROM coche WHERE bastidor = '{$_SESSION["bastidor"]}'
        ")
            ->obtenerDatos();

        $coche = new Coche([
            'bastidor' => $datos[0]['bastidor'],
            'marca' => $datos[0]['marca'],
            'modelo' => $datos[0]['modelo'],
            'cilindrada' => $datos[0]['cilindrada'],
        ]);

        require_once '_form.php';
    }
    ?>
</body>

</html>