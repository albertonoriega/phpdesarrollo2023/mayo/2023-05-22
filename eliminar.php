<?php
spl_autoload_register(function ($nombreClase) {
    require_once "$nombreClase.php";
});

use clases\elementos\Coche;
use clases\librerias\Conexion;

session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="estilos/main.css">
</head>

<body>

    <?php

    // Cuando se pulsa eliminar y tenemos bastidor por la URL
    if (isset($_GET['bastidor'])) {
        $conexion = new Conexion([
            'baseDatos' => 'concesionario',
        ]);
        // Generamos una variable donde guardamos los campos del bastidor que pasamos
        $datos = $conexion->consulta("SELECT * FROM coche WHERE bastidor= '{$_GET["bastidor"]}' ")->obtenerDatos()[0];

        // Generamos un objeto con los datos
        $coche = new Coche([
            'bastidor' => $datos['bastidor'],
            'marca' => $datos['marca'],
            'modelo' => $datos['modelo'],
            'cilindrada' => $datos['cilindrada'],
        ]);

        $_SESSION['bastidor'] = $_GET['bastidor'];
        require_once '_ver.php';

        // Si se ha pulsado el boton eliminar de la subvista ver
    } elseif (isset($_GET['eliminar'])) {

        $conexion = new Conexion([
            'baseDatos' => 'concesionario',
        ]);
        // Generamos una variable donde guardamos los campos del bastidor que pasamos
        $datos = $conexion->consulta("SELECT * FROM coche WHERE bastidor= '{$_SESSION["bastidor"]}' ")->obtenerDatos()[0];

        // Generamos un objeto con los datos
        $coche = new Coche([
            'bastidor' => $datos['bastidor'],
            'marca' => $datos['marca'],
            'modelo' => $datos['modelo'],
            'cilindrada' => $datos['cilindrada'],
        ]);

        $coche->eliminar($conexion);

    ?>
        <div class="botonesVer">
            <p>El registro se ha eliminado</p>
            <a href="index.php">Volver al inicio</a>
        </div>
    <?php
    } else {
        echo "No tengo los datos del coche a eliminar";
    }
    ?>
</body>

</html>