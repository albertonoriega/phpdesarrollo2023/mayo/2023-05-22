﻿DROP DATABASE IF EXISTS concesionario;

CREATE DATABASE concesionario;

USE concesionario;

CREATE TABLE coche(
bastidor varchar(100),
marca varchar(100),
modelo varchar (100),
cilindrada int,
PRIMARY KEY (bastidor)
);