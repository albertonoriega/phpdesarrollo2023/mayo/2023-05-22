<?php

spl_autoload_register(function ($nombreClase) {
    require_once "$nombreClase.php";
});

use clases\elementos\Coche;
use clases\librerias\Conexion;
use clases\librerias\Utilidades;

// Establecer la conexión
$conexion = new Conexion([
    'baseDatos' => 'concesionario',
]);


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="estilos/main.css">
    <script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.js"></script>

</head>

<body>
    <?php
    // Consulta para listar
    $datos = $conexion->consulta('Select * from coche')->obtenerDatos();

    // Cargamos el menu
    require_once '_menu.php';

    //Usando el gridview, método estático de la clase Utilidades
    echo Utilidades::gridView($datos, [],  'bastidor');


    // // Cambiamos la cilindrada del primer registro
    // $coches[0]->setCilindrada(1666);
    // // Lo actualizamos en la BBDD
    // $coches[0]->actualizar($conexion);
    ?>
</body>

</html>