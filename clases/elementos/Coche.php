<?php

namespace clases\elementos;

class Coche
{
    public  $marca;
    public  $modelo;
    public  $cilindrada;
    public  $bastidor;
    private $bastidorViejo; // para la variable de sesion


    /**
     * __construct
     *
     * @param  array $datos debes pasar un array asociativo con las propiedades a inicializar
     * son marca, modelo, cilindrada y bastidor
     * @return void
     */
    public function __construct($datos = [])
    {
        $this->marca = $datos['marca'] ?? '';
        $this->modelo = $datos['modelo'] ?? '';
        $this->cilindrada = $datos['cilindrada'] ?? 0;
        $this->bastidor = $datos['bastidor'] ?? '';
        $this->bastidorViejo = $datos['bastidorViejo'] ?? '';
    }

    public function __toString()
    {
        $salida = "<ul>";
        $salida .= "<li> Marca: {$this->marca} </li>";
        $salida .= "<li> Modelo: {$this->modelo} </li>";
        $salida .= "<li> Cilindrada: {$this->cilindrada} </li>";
        $salida .= "<li> Bastidor: {$this->bastidor} </li>";
        $salida .= "</ul>";
        return $salida;
    }

    // GETTERS
    public function getMarca()
    {
        return $this->marca;
    }

    public function getModelo()
    {
        return $this->modelo;
    }

    public function getCilindrada()
    {
        return $this->cilindrada;
    }

    public function getBastidor()
    {
        return $this->bastidor;
    }

    // SETTERS
    public function setMarca($marca)
    {
        $this->marca = $marca;

        return $this;
    }

    public function setModelo($modelo)
    {
        $this->modelo = $modelo;

        return $this;
    }

    public function setCilindrada($cilindrada)
    {
        $this->cilindrada = $cilindrada;

        return $this;
    }

    public function setBastidor($bastidor)
    {
        if (!empty($bastidor)) {
            $this->bastidor = $bastidor;
        }
        return $this;
    }

    public function dibujarTabla()
    {
        ob_start();
?>
        <table class="contenidoTablas" style="text-align: center; border-collapse: collapse;">
            <thead>
                <tr>
                    <td>Marca</td>
                    <td>Modelo</td>
                    <td>Cilindrada</td>
                    <td>Bastidor</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td> <?= $this->getMarca() ?></td>
                    <td> <?= $this->getModelo() ?></td>
                    <td> <?= $this->getCilindrada() ?></td>
                    <td> <?= $this->getBastidor() ?></td>
                </tr>
            </tbody>
        </table>
<?php
        return ob_get_clean();
    }

    public function insertar($conexion)
    {
        $conexion->consulta("INSERT INTO coche VALUES ('{$this->bastidor}', '{$this->marca}', '{$this->modelo}' , {$this->cilindrada})");
    }

    /**
     * actualizar Funcion para actualizar todos los campos de un registro
     *
     * @param  mixed $conexion conexion a la BBDD
     * @return void
     */
    public function actualizar($conexion)
    {
        $conexion->consulta("UPDATE coche SET marca = '{$this->marca}', modelo = '{$this->modelo}', cilindrada = {$this->cilindrada}, bastidor= '{$this->bastidor}'  WHERE bastidor='{$this->bastidorViejo}'");
    }

    public function eliminar($conexion)
    {
        $conexion->consulta("DELETE FROM coche WHERE bastidor='{$this->bastidor}'");
    }
}
