<?php

namespace clases\librerias;

use mysqli;

class Conexion
{
    private $conexion;
    private $consulta;

    /**
     * __construct Crea una conexión con un servidor de BBDD
     *
     * @param  array $propiedadesConexion debes pasar los parametros como un array asociativo
     * las claves a inicializar son equipo, usuario, contrasena, baseDatos, puerto
     * @return void
     */
    public function __construct($propiedadesConexion)
    {
        // Siempre le vamos a tener que pasar la base de datos a conectar
        // Los otros datos podemos pasarselos o no
        // Si no los pasamos, vamos a asignarles unos valores por defecto usando if u operadores ternarios

        /*
        Usamos IF
        if (!array_key_exists('equipo', $propiedadesConexion)) {
            // Coge el valor por defecto el localhost
            $propiedadesConexion['127.0.0.1'];
        }
         */

        // Usamos un OPERADOR TERNARIO
        // Si no exsite el indice del array, devolvemos localhost. Si existe devolvemos el valor que se ha pasado

        // operador ternario completo
        // $propiedadesConexion['equipo'] = !array_key_exists('equipo', $propiedadesConexion) ? 'localhost' : $propiedadesConexion['equipo'];

        //Ternario contraido (usando isset no array_key_exists)

        //$propiedadesConexion['equipo'] = $propiedadesConexion['equipo'] ?? '127.0.0.1';

        $this->conexion = new mysqli(
            $propiedadesConexion['equipo'] ?? '127.0.0.1',
            $propiedadesConexion['usuario'] ?? 'root',
            $propiedadesConexion['contrasena'] ?? '',
            $propiedadesConexion['baseDatos'],
            $propiedadesConexion['puerto'] ?? 3306,
        );
    }

    public function consulta($sql)
    {
        $this->consulta = $this->conexion->query($sql);
        return $this; // fluent 
    }

    public function obtenerDatos()
    {
        return $this->consulta->fetch_all(MYSQLI_ASSOC);
    }
}
